package com.dharbor.nexus.trn.nexustrn.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author Willy Sanchez
 */
@Configuration
@ComponentScan("com.dharbor.nexus.trn.business.forum.business")
@EnableAutoConfiguration
public class Config {
}
