package com.dharbor.nexus.trn.nexustrn.service;

import com.dharbor.nexus.trn.business.forum.business.model.domain.BaseComment;
import com.dharbor.nexus.trn.business.forum.business.model.domain.CommentComposite;
import com.dharbor.nexus.trn.business.forum.business.model.domain.Post;
import com.dharbor.nexus.trn.business.forum.business.model.repository.CommentRepository;
import com.dharbor.nexus.trn.business.forum.business.model.repository.PostRepository;
import com.dharbor.nexus.trn.nexustrn.controller.input.CommentInput;
import com.dharbor.nexus.trn.nexustrn.controller.response.CommentResponse;
import com.dharbor.nexus.trn.nexustrn.exception.CommentNotFoundException;
import com.dharbor.nexus.trn.nexustrn.exception.PostNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * @author Willy Sanchez
 */

@Service
@Scope("prototype")
public class CommentCreateService {

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private PostRepository postRepository;

    public CommentResponse execute(CommentInput input) {
        Post post = findPost(input.getPostId());
        CommentComposite baseComment = findComment(input.getReplyId());
        if (post != null) {
            CommentComposite instance = composeComment(post, baseComment, input);
            return composeResponse(commentRepository.save(instance));
        }
        return null;
    }

    private CommentComposite composeComment(Post post, BaseComment baseComment, CommentInput input) {
        CommentComposite instance = new CommentComposite();
        instance.setComment(input.getComment());
        instance.setNickName(input.getNickName());
        instance.setPost(post);
        instance.setCreatedDate(new Date());
        instance.setBaseComment(baseComment);

        return instance;
    }

    private Post findPost(Long postId) {
        return postRepository.findById(postId)
                .orElseThrow(() -> new PostNotFoundException("Unable locate post to postId: " + postId));
    }

    private CommentComposite findComment(Long replyId) {
        if (replyId != null) {
            return commentRepository.findById(replyId)
                    .orElseThrow(() -> new CommentNotFoundException("Unable locate comment to commentId: " + replyId));
        }

        return null;
    }

    private CommentResponse composeResponse(BaseComment comment) {
        CommentResponse response = new CommentResponse();
        response.setComment(comment.getComment());
        response.setNickName(comment.getNickName());

        return response;
    }
}
