package com.dharbor.nexus.trn.nexustrn.controller.input;

/**
 * @author Willy Sanchez
 */
public class PostInput {

    private String message;

    private String nickName;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }
}
