package com.dharbor.nexus.trn.nexustrn.service;

import com.dharbor.nexus.trn.business.forum.business.model.domain.Post;
import com.dharbor.nexus.trn.business.forum.business.model.repository.PostRepository;
import com.dharbor.nexus.trn.nexustrn.controller.input.PostInput;
import com.dharbor.nexus.trn.nexustrn.controller.response.PostResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * @author Willy Sanchez
 */
@Service
@Scope("prototype")
public class PostCreateService {

    @Autowired
    private PostRepository repository;

    public PostResponse CreatePost(PostInput input) {
        Post instance = composePost(input);
        return composeResponse(repository.save(instance));
    }

    private Post composePost(PostInput input) {
        Post instance = new Post();
        instance.setNickName(input.getNickName());
        instance.setMessage(input.getMessage());
        instance.setCreatedDate(new Date());

        return instance;
    }

    private PostResponse composeResponse(Post instance) {
        PostResponse response = new PostResponse();
        response.setMessage(instance.getMessage());
        response.setNickName(instance.getNickName());

        return response;
    }
}
