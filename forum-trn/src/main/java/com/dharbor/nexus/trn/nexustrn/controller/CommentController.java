package com.dharbor.nexus.trn.nexustrn.controller;

import com.dharbor.nexus.trn.nexustrn.controller.input.CommentInput;
import com.dharbor.nexus.trn.nexustrn.controller.response.CommentResponse;
import com.dharbor.nexus.trn.nexustrn.resources.CommentResource;
import com.dharbor.nexus.trn.nexustrn.service.CommentByPostIdService;
import com.dharbor.nexus.trn.nexustrn.service.CommentCreateService;
import com.dharbor.nexus.trn.nexustrn.service.CommentGetByIdService;
import com.dharbor.nexus.trn.nexustrn.service.CommnetAllService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.RequestScope;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.List;

/**
 * @author Willy Sanchez
 */
@Api(
        tags = "Rest-comment-controller",
        description = "Operations over comments"
)
@RequestMapping(value = Constants.BasePath.COMMENTS, produces = "application/hal+json")
@RestController
@RequestScope
public class CommentController {

    @Autowired
    private CommentCreateService commentCreateService;

    @Autowired
    private CommnetAllService commnetAllService;

    @Autowired
    private CommentGetByIdService commentGetByIdService;

    @ApiOperation(
            value = "Create a comment"
    )
    @ApiResponses({
            @ApiResponse(
                    code = 200,
                    message = "Comment created"
            ),
            @ApiResponse(
                    code = 404,
                    message = "Post not exist on data base.\n\n"
                            + "Comment not exist on data base"
            )
    })
    @RequestMapping(method = RequestMethod.POST)
    public CommentResponse createComment(@RequestBody CommentInput input) {

        return commentCreateService.execute(input);
    }


    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Resources<CommentResource>> getCommnets() {

        List<CommentResource> collection = commnetAllService.getComments();
        Resources<CommentResource> resources = new Resources<>(collection);
        String uriString = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
        resources.add(new Link(uriString,"self"));
        return ResponseEntity.ok(resources);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public ResponseEntity<CommentResource> getById(@PathVariable Long id){
        return ResponseEntity.ok(commentGetByIdService.getById(id));
    }
}