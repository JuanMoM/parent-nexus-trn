package com.dharbor.nexus.trn.nexustrn.service;

import com.dharbor.nexus.trn.business.forum.business.model.domain.CommentComposite;
import com.dharbor.nexus.trn.business.forum.business.model.domain.CommentComposite_;
import com.dharbor.nexus.trn.business.forum.business.model.repository.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Willy Sanchez
 */
@Service
@Scope("prototype")
public class CommentByPostIdService {

    @Autowired
    private CommentRepository repository;

    public List<CommentComposite> getByPostId(Long postId) {
        return repository.findAll();
    }

    private Specification<CommentComposite> buildQuery(Long postId) {
        return (root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            query.where(criteriaBuilder.equal(root.get(CommentComposite_.postId), postId));

            return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}
