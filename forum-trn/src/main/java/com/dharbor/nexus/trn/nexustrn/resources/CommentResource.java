package com.dharbor.nexus.trn.nexustrn.resources;

import com.dharbor.nexus.trn.business.forum.business.model.domain.CommentComposite;
import com.dharbor.nexus.trn.nexustrn.controller.CommentController;
import lombok.Getter;
import org.springframework.hateoas.ResourceSupport;

import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * @author Willy Sanchez
 */

public class CommentResource extends ResourceSupport {

    private CommentComposite comment;

    public CommentResource(final CommentComposite comment) {
        this.comment = comment;
        final long id = comment.getId();
        add(linkTo(CommentController.class).withRel("coment"));

        add(linkTo(methodOn(CommentController.class).getById(id)).withSelfRel());
    }

    public CommentComposite getComment() {
        return comment;
    }
}
