package com.dharbor.nexus.trn.nexustrn.controller;

/**
 * @author Willy Sanchez
 */
public final class Constants {
    private Constants() {
    }

    public static final class BasePath {
        public static final String POSTS = "/posts";
        public static final String COMMENTS = "/comments";
    }
}
