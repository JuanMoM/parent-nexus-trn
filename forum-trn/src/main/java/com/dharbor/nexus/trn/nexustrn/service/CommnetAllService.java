package com.dharbor.nexus.trn.nexustrn.service;

import com.dharbor.nexus.trn.business.forum.business.model.repository.CommentRepository;
import com.dharbor.nexus.trn.nexustrn.resources.CommentResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @autor Juan Montaño Mamani
 */
@Service
public class CommnetAllService {

    @Autowired
    CommentRepository repository;

    public List<CommentResource> getComments() {
        return repository.findAll()
                .stream()
                .map(CommentResource::new)
                .collect(Collectors.toList());
    }
}
