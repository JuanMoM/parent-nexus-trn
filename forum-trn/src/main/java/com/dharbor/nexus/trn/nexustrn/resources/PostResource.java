package com.dharbor.nexus.trn.nexustrn.resources;

import com.dharbor.nexus.trn.business.forum.business.model.domain.Post;
import com.dharbor.nexus.trn.nexustrn.controller.CommentController;
import com.dharbor.nexus.trn.nexustrn.controller.PostController;
import lombok.Getter;
import org.springframework.hateoas.ResourceSupport;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * @author Willy Sanchez
 */
@Getter
public class PostResource extends ResourceSupport {

    private final Post post;

    public PostResource(final Post post) {
        this.post = post;
        final Long id = post.getPostId();
        add(linkTo(PostController.class).withRel("posts"));
        add(linkTo(methodOn(PostController.class).readPostById(id)).withSelfRel());
        add(linkTo(methodOn(CommentController.class).getCommnets()).withSelfRel());
        //add(linkTo(methodOn(CommentController.class)));
    }
}