package com.dharbor.nexus.trn.business.forum.business;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ForumBusinessApplicationTests {

    @Test
    public void contextLoads() {
    }

}
