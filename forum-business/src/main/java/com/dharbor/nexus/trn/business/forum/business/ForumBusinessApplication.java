package com.dharbor.nexus.trn.business.forum.business;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ForumBusinessApplication {

    public static void main(String[] args) {
        SpringApplication.run(ForumBusinessApplication.class, args);
    }

}
