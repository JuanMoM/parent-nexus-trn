package com.dharbor.nexus.trn.business.forum.business.model.domain;



import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.Date;

/**
 * @author Willy Sanchez
 */
@Entity
@Table(name = Constants.BaseCommentTable.NAME)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public abstract class BaseComment {

    @Id
    @Column(name = Constants.BaseCommentTable.Id.NAME, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = Constants.BaseCommentTable.Comment.NAME, nullable = false)
    private String comment;

    @Column(name = Constants.BaseCommentTable.NickNameCommentator.NAME, nullable = false)
    private String nickName;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = Constants.BaseCommentTable.PostId.NAME, referencedColumnName = Constants.PostTable.Id.NAME, nullable = false)
    private Post post;

    @Column(name = Constants.BaseCommentTable.PostId.NAME, insertable = false, updatable = false, nullable = false)
    private Long postId;

    @Temporal(TemporalType.DATE)
    @Column(name = Constants.BaseCommentTable.CreatedDate.NAME, nullable = false)
    private Date createdDate;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Long getPostId() {
        return postId;
    }

    public void setPostId(Long postId) {
        this.postId = postId;
    }
}
