package com.dharbor.nexus.trn.business.forum.business.controller;

import org.springframework.hateoas.ResourceSupport;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * @author Willy Sanchez
 */
@RestController
@RequestMapping(value = "/posts", produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
public class RootController {

    @GetMapping("/")
    ResponseEntity<ResourceSupport> root() {
        ResourceSupport resourceSupport = new ResourceSupport();
        resourceSupport.add(linkTo(methodOn(RootController.class).root()).withSelfRel());

        return ResponseEntity.ok(resourceSupport);
    }

}
