package com.dharbor.nexus.trn.business.forum.business.service;


import com.dharbor.nexus.trn.business.forum.business.model.domain.Post;
import com.dharbor.nexus.trn.business.forum.business.model.domain.Post_;
import com.dharbor.nexus.trn.business.forum.business.model.repository.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;


/**
 * @autor Juan Montaño Mamani
 */
@Service
@Scope("prototype")
public class PostListService {

    @Autowired
    private PostRepository repository;

    public Page<Post> createPageable(Integer page, Integer size) {
        Pageable pageable = PageRequest.of(page, size);
        return repository.findAll(builtQuery(), pageable);
    }

    private Specification<Post> builtQuery() {
        return (root, criteriaQuery, criteriaBuilder) -> {

            List<Predicate> predicates = new ArrayList<>();
            criteriaQuery.orderBy(criteriaBuilder.desc(root.get(Post_.createdDate)));

            return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}
