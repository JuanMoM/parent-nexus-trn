package com.dharbor.nexus.trn.business.forum.business.controller;

import com.dharbor.nexus.trn.business.forum.business.controller.input.CommentInput;
import com.dharbor.nexus.trn.business.forum.business.controller.response.CommentResponse;
import com.dharbor.nexus.trn.business.forum.business.model.domain.CommentComposite;
import com.dharbor.nexus.trn.business.forum.business.service.CommentCreateService;
import com.dharbor.nexus.trn.business.forum.business.service.CommentGetByIdService;
import com.dharbor.nexus.trn.business.forum.business.service.CommnetAllService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.RequestScope;

import java.util.List;

/**
 * @author Willy Sanchez
 */
@Api(
        tags = "Rest-comment-controller",
        description = "Operations over comments"
)
@RequestMapping(value = Constants.BasePath.COMMENTS, produces = "application/hal+json")
@RestController
@RequestScope
public class CommentController {

    @Autowired
    private CommentCreateService commentCreateService;

    @Autowired
    private CommnetAllService commnetAllService;

    @Autowired
    private CommentGetByIdService commentGetByIdService;

    @ApiOperation(
            value = "Create a comment"
    )
    @ApiResponses({
            @ApiResponse(
                    code = 200,
                    message = "Comment created"
            ),
            @ApiResponse(
                    code = 404,
                    message = "Post not exist on data base.\n\n"
                            + "Comment not exist on data base"
            )
    })
    @RequestMapping(method = RequestMethod.POST)
    public CommentResponse createComment(@RequestBody CommentInput input) {

        return commentCreateService.execute(input);
    }


    @RequestMapping(method = RequestMethod.GET)
    public List<CommentComposite> getCommnets() {

        return commnetAllService.getComments();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public CommentComposite getById(@PathVariable Long id) {
        return commentGetByIdService.getById(id);
    }
}