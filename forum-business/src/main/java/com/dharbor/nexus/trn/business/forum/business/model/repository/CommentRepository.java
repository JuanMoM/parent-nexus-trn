package com.dharbor.nexus.trn.business.forum.business.model.repository;

import com.dharbor.nexus.trn.business.forum.business.model.domain.CommentComposite;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * @author Willy Sanchez
 */
@RepositoryRestResource(collectionResourceRel = "comment", path = "comment")
public interface CommentRepository extends JpaRepository<CommentComposite, Long>, JpaSpecificationExecutor {
}
