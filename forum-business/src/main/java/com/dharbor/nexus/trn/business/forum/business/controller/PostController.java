package com.dharbor.nexus.trn.business.forum.business.controller;

import com.dharbor.nexus.trn.business.forum.business.controller.input.PostInput;
import com.dharbor.nexus.trn.business.forum.business.controller.response.PostResponse;
import com.dharbor.nexus.trn.business.forum.business.model.domain.Post;
import com.dharbor.nexus.trn.business.forum.business.service.PostCreateService;
import com.dharbor.nexus.trn.business.forum.business.service.PostListAllService;
import com.dharbor.nexus.trn.business.forum.business.service.PostListService;
import com.dharbor.nexus.trn.business.forum.business.service.ReadPostById;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.RequestScope;

import java.util.List;

/**
 * @author Willy Sanchez
 */
@Api(
        tags = "Rest-post-controller",
        description = "Operations over post"
)
@RequestMapping(value = Constants.BasePath.POSTS, produces = {MediaType.APPLICATION_JSON_VALUE, "application/hal+json"})
@RestController
@RequestScope

public class PostController {

    @Autowired
    private PostCreateService postCreateService;

    @Autowired
    private PostListService postListService;

    @Autowired
    private ReadPostById readPostById;

    @Autowired
    private PostListAllService postListAllService;

    @ApiOperation(
            value = "list of post falling"
    )
//    @RequestMapping(method = RequestMethod.GET, value = "/pageable")
//    public Post getPostDesc(@RequestParam(value = "page") Integer page, @RequestParam(value = "size") Integer size) {
//        final Resources<PostResource> resources = new Resources<>(postListService.createPageable(page, size).stream().map(PostResource::new).collect(Collectors.toList()));
//        return ResponseEntity.ok(resources);
//    }

    @RequestMapping(method = RequestMethod.GET)
    public List<Post> all() {
        return postListAllService.execute();
    }

    @ApiOperation(
            value = "Create a Post"
    )
    @ApiResponses({
            @ApiResponse(code = 201, message = "Post created"),
            @ApiResponse(code = 404, message = "Not Post Created")
    })
    @RequestMapping(method = RequestMethod.POST)
    public PostResponse createPost(@RequestBody PostInput input) {
        PostResponse p = postCreateService.CreatePost(input);

        return p;
    }

    @RequestMapping(
            value = "/{postId}",
            method = RequestMethod.GET)
    public Post readPostById(@PathVariable Long postId) {
        return readPostById.execute(postId);
    }
}
