package com.dharbor.nexus.trn.business.forum.business.model.domain;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.util.Date;

/**
 * @autor Juan Montaño Mamani
 */
@StaticMetamodel(Post.class)
public class Post_ {

    public static volatile SingularAttribute<Post, Long> id;
    public static volatile SingularAttribute<Post, String> message;
    public static volatile SingularAttribute<Post, String> nickName;
    public static volatile SingularAttribute<Post, Date> createdDate;
}
