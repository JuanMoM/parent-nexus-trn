package com.dharbor.nexus.trn.business.forum.business.model.domain;

/**
 * @author Willy Sanchez
 */
final class Constants {

    public Constants() {
    }

    public static final class BaseCommentTable {

        public static final String NAME = "comment_table";

        public static final class Id {

            public static final String NAME = "commentid";
        }

        public static final class Comment {

            public static final String NAME = "comment";
        }

        public static final class NickNameCommentator {

            public static final String NAME = "nicknamecommentator";
        }

        public static final class PostId {

            public static final String NAME = "postid";
        }

        public static final class CreatedDate {

            public static final String NAME = "createddate";
        }
    }

    public static final class CommentCompositeTable {

        public static final String NAME = "commentcomposite_table";

        public static final class Id {

            public static final String NAME = "commentcompositeid";
        }

        public static final class BaseComment {

            public static final String NAME = "replycommentid";
        }
    }

    public static final class PostTable {

        public static final String NAME = "post_table";

        public static final class Id {

            public static final String NAME = "postid";
        }

        public static final class Message {

            public static final String NAME = "message";
        }

        public static final class NickNamePoster {

            public static final String NAME = "nicknameposter";
        }

        public static final class CreatedDate {

            public static final String NAME = "createddate";
        }
    }
}
