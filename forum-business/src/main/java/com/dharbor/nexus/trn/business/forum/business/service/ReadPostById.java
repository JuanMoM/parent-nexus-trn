package com.dharbor.nexus.trn.business.forum.business.service;

import com.dharbor.nexus.trn.business.forum.business.model.domain.Post;
import com.dharbor.nexus.trn.business.forum.business.model.repository.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

/**
 * @author Willy Sanchez
 */
@Service
@Scope("prototype")
public class ReadPostById {

    @Autowired
    private PostRepository repository;

    public Post execute(Long postId) {
        return repository.findById(postId).orElseThrow(null);
    }
}
