package com.dharbor.nexus.trn.business.forum.business.service;

import com.dharbor.nexus.trn.business.forum.business.model.domain.CommentComposite;
import com.dharbor.nexus.trn.business.forum.business.model.repository.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @autor Juan Montaño Mamani
 */
@Service
public class CommnetAllService {

    @Autowired
    CommentRepository repository;

    public List<CommentComposite> getComments() {
        return repository.findAll();
    }
}
