package com.dharbor.nexus.trn.business.forum.business.model.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

/**
 * @author Willy Sanchez
 */
@Entity
@Table(name = Constants.CommentCompositeTable.NAME)
@PrimaryKeyJoinColumns({
        @PrimaryKeyJoinColumn(
                name = Constants.CommentCompositeTable.Id.NAME,
                referencedColumnName = Constants.BaseCommentTable.Id.NAME
        )
})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class CommentComposite extends BaseComment {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = Constants.CommentCompositeTable.BaseComment.NAME, referencedColumnName = Constants.BaseCommentTable.Id.NAME)
    private BaseComment baseComment;

    public BaseComment getBaseComment() {
        return baseComment;
    }

    public void setBaseComment(BaseComment baseComment) {
        this.baseComment = baseComment;
    }
}
