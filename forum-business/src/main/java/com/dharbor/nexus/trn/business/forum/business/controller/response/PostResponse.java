package com.dharbor.nexus.trn.business.forum.business.controller.response;

import org.springframework.hateoas.ResourceSupport;

/**
 * @author Willy Sanchez
 */
public class PostResponse extends ResourceSupport {

    private String message;

    private String nickName;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }
}
