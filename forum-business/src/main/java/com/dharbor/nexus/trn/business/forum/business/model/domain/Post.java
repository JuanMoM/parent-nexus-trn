package com.dharbor.nexus.trn.business.forum.business.model.domain;

import org.springframework.hateoas.ResourceSupport;

import javax.persistence.*;
import java.util.Date;

/**
 * @author Willy Sanchez
 */
@Entity
@Table(name = Constants.PostTable.NAME)
public class Post extends ResourceSupport{

    @Id
    @Column(name = Constants.PostTable.Id.NAME, nullable = false, unique = true)
    @GeneratedValue(strategy =GenerationType.SEQUENCE)
    private Long id;

    @Column(name = Constants.PostTable.Message.NAME, nullable = false)
    private String message;

    @Column(name = Constants.PostTable.NickNamePoster.NAME)
    private String nickName;

    @Temporal(TemporalType.DATE)
    @Column(name = Constants.PostTable.CreatedDate.NAME)
    private Date createdDate;

    public Long getPostId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }
}
