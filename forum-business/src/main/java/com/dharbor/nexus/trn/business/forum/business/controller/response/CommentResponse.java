package com.dharbor.nexus.trn.business.forum.business.controller.response;

/**
 * @author Willy Sanchez
 */
public class CommentResponse {

    private String comment;

    private String nickName;

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

}
