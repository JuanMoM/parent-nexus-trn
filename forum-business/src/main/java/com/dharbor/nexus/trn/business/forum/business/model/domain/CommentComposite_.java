package com.dharbor.nexus.trn.business.forum.business.model.domain;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.util.Date;

/**
 * @author Willy Sanchez
 */
@StaticMetamodel(CommentComposite.class)
public class CommentComposite_ {

    public static volatile SingularAttribute<CommentComposite, Long> id;
    public static volatile SingularAttribute<CommentComposite, String> comment;
    public static volatile SingularAttribute<CommentComposite, String> nickName;
    public static volatile SingularAttribute<CommentComposite, Post> post;
    public static volatile SingularAttribute<CommentComposite, Long> postId;
    public static volatile SingularAttribute<CommentComposite, Date> createdDate;
    public static volatile SingularAttribute<CommentComposite, BaseComment> baseComment;
}
